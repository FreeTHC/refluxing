# A static TOV star
# An example for the Einstein Toolkit; see <http://einsteintoolkit.org/>

ActiveThorns = "
        ADMBase
        ADMCoupling
        ADMMacros
        AEILocalInterp
        Boundary
        Carpet
        CarpetIOASCII
        CarpetIOBasic
        CarpetIOScalar
        CarpetInterp
        CarpetLib
        CarpetReduce
        CarpetRegrid2
        CartGrid3d
        Constants
        CoordGauge
        Coordbase
        EOS_Omni
        Formaline
        GRHydro
        GenericFD
        HydroBase
        IOUtil
        InitBase
        LoopControl
        ML_ADMConstraints
        ML_BSSN
        ML_BSSN_Helper
        MoL
        NaNChecker
        NewRad
        ReflectionSymmetry
        Refluxing
        SpaceMask
        StaticConformal
        SymBase
        TOVSolver
        Time
        TmunuBase
"



################################################################################
################################################################################
# Physics and numerics
################################################################################
################################################################################

################################################################################
# Termination condition
################################################################################

Cactus::terminate       = "time"
Cactus::cctk_final_time = 10.0   # 1000.0
#Cactus::cctk_itlast     = 256

################################################################################
# Initial condition
################################################################################

InitBase::initial_data_setup_method = "init_some_levels"

ADMBase::initial_data    = "tov"
ADMBase::initial_lapse   = "tov"
ADMBase::initial_shift   = "tov"
ADMBase::initial_dtlapse = "zero"
ADMBase::initial_dtshift = "zero"

TOVSolver::TOV_Rho_Central[0] =   1.28e-3
TOVSolver::TOV_Gamma[0]       =   2.0
TOVSolver::TOV_K[0]           = 100.0

################################################################################
# Time evolution: spacetime
################################################################################

MoL::ODE_Method             = "RK4"
MoL::MoL_Intermediate_Steps = 4
MoL::MoL_Num_Scratch_Levels = 1
Time::dtfac                 = 0.25

ADMBase::evolution_method        = "ML_BSSN"
ADMBase::lapse_evolution_method  = "ML_BSSN"
ADMBase::shift_evolution_method  = "ML_BSSN"
ADMBase::dtlapse_evolution_method= "ML_BSSN"
ADMBase::dtshift_evolution_method= "ML_BSSN"

ML_BSSN::timelevels = 3

# Lapse Condition: \partial_t alpha = - alpha K
# Shift Condition: \partial_t beta^i = 0
ML_BSSN::harmonicN           = 1      # 1+log
ML_BSSN::harmonicF           = 1.0    # 1+log
ML_BSSN::LapseACoeff         = 1.0
ML_BSSN::ShiftBCoeff         = 1.0
ML_BSSN::ShiftGammaCoeff     = 0.0
ML_BSSN::AlphaDriver         = 0.0
ML_BSSN::BetaDriver          = 0.0
ML_BSSN::LapseAdvectionCoeff = 0.0
ML_BSSN::ShiftAdvectionCoeff = 0.0

ML_BSSN::MinimumLapse        = 1.0e-8

ML_BSSN::my_initial_boundary_condition = "extrapolate-gammas"
ML_BSSN::my_rhs_boundary_condition     = "NewRad"

ML_BSSN::ML_log_confac_bound = "none"
ML_BSSN::ML_metric_bound     = "none"
ML_BSSN::ML_Gamma_bound      = "none"
ML_BSSN::ML_trace_curv_bound = "none"
ML_BSSN::ML_curv_bound       = "none"
ML_BSSN::ML_lapse_bound      = "none"
ML_BSSN::ML_dtlapse_bound    = "none"
ML_BSSN::ML_shift_bound      = "none"
ML_BSSN::ML_dtshift_bound    = "none"

################################################################################
# Time evolution: hydro
################################################################################

HydroBase::evolution_method  = "GRHydro"
hydrobase::prolongation_type = "Lagrange"
HydroBase::timelevels        = 3

TmunuBase::stress_energy_storage           = "yes"
TmunuBase::stress_energy_at_RHS            = "yes"
TmunuBase::prolongation_type               = "none"
TmunuBase::support_old_CalcTmunu_mechanism = "no"

SpaceMask::use_mask = "yes"

GRHydro::riemann_solver    = "Marquina"
GRHydro::GRHydro_eos_type  = "Polytype"
GRHydro::GRHydro_eos_table = "2D_Polytrope"
GRHydro::recon_method      = "ppm"
GRHydro::GRHydro_stencil   = 3
GRHydro::bound             = "none"
GRHydro::rho_abs_min       = 1.e-10

EOS_Omni::poly_gamma =   2.0
EOS_Omni::poly_k     = 100.0

Refluxing::refluxing_debug_variables = yes

################################################################################
# Analysis
################################################################################

NaNChecker::check_every     = 1
NaNChecker::action_if_found = "just warn"
NaNChecker::check_vars      = "
        ADMBase::metric
        ADMBase::curv
        ADMBase::lapse
        ADMBase::shift
        HydroBase::rho
        HydroBase::vel
        HydroBase::eps
        HydroBase::press
"



################################################################################
################################################################################
# Grid structure
################################################################################
################################################################################

Carpet::max_refinement_levels    = 10
driver::ghost_size               =  3
Carpet::prolongation_order_space =  1
Carpet::prolongation_order_time  =  2

Carpet::init_3_timelevels        = "no"
Carpet::init_fill_timelevels     = "yes"

Carpet::poison_new_timelevels = "yes"
Carpet::check_for_poison      = "no"
CarpetLib::poison_new_memory  = "yes"
CarpetLib::poison_value       = 114

CarpetReduce::verbose = yes

################################################################################
# Domain
################################################################################

Carpet::domain_from_coordbase = "yes"
Carpet::enable_all_storage    = no
Carpet::use_buffer_zones      = "yes"
Carpet::refinement_centering  = "cell"

CartGrid3D::type         = "coordbase"
CartGrid3D::avoid_origin = "no"
CoordBase::xmin          =   0.0
CoordBase::ymin          =   0.0
CoordBase::zmin          =   0.0
CoordBase::xmax          = 240.0
CoordBase::ymax          = 240.0
CoordBase::zmax          = 240.0
CoordBase::dx            =   8
CoordBase::dy            =   8
CoordBase::dz            =   8

################################################################################
# Boundaries
################################################################################

CoordBase::boundary_size_x_lower      = 3
CoordBase::boundary_size_y_lower      = 3
CoordBase::boundary_size_z_lower      = 3
CoordBase::boundary_size_x_upper      = 3
CoordBase::boundary_size_y_upper      = 3
CoordBase::boundary_size_z_upper      = 3
CoordBase::boundary_staggered_x_lower = "yes"
CoordBase::boundary_staggered_y_lower = "yes"
CoordBase::boundary_staggered_z_lower = "yes"
CoordBase::boundary_staggered_x_upper = "yes"
CoordBase::boundary_staggered_y_upper = "yes"
CoordBase::boundary_staggered_z_upper = "yes"

################################################################################
# Symmetries (octant mode)
################################################################################

ReflectionSymmetry::reflection_x   = "yes"
ReflectionSymmetry::reflection_y   = "yes"
ReflectionSymmetry::reflection_z   = "yes"
ReflectionSymmetry::avoid_origin_x = "yes"
ReflectionSymmetry::avoid_origin_y = "yes"
ReflectionSymmetry::avoid_origin_z = "yes"

################################################################################
# Refinement hierarchy
################################################################################

CarpetRegrid2::verbose        = "yes"
CarpetRegrid2::regrid_every   = 0   # static
CarpetRegrid2::snap_to_coarse = "yes"

CarpetRegrid2::num_centres  = 1
CarpetRegrid2::num_levels_1 =   5
CarpetRegrid2::radius_1[1]  = 120.0
CarpetRegrid2::radius_1[2]  =  60.0
CarpetRegrid2::radius_1[3]  =  30.0
CarpetRegrid2::radius_1[4]  =  15.0



################################################################################
################################################################################
# Output
################################################################################
################################################################################

IO::out_dir  = $parfile

################################################################################
# Screen output
################################################################################

IOBasic::outInfo_every = 1
IOBasic::outInfo_vars  = "
        ADMBase::lapse
        HydroBase::rho
"

################################################################################
# Norms
################################################################################

IOScalar::outScalar_every = 32
IOScalar::one_file_per_group = yes
IOScalar::outScalar_vars  = "
        ADMBase::lapse
        ADMBase::shift
        ADMBase::metric
        ADMBase::curv
        HydroBase::rho
        HydroBase::vel
        HydroBase::eps
        HydroBase::press
        GRHydro::dens
        GRHydro::scon
        GRHydro::tau
        ML_ADMConstraints::ML_Ham
        ML_ADMConstraints::ML_Mom
        Refluxing::flux_weight_fine
        Refluxing::flux_weight_coarse
"

################################################################################
# ASCII output
################################################################################

IOASCII::out1D_every            = 256
IOASCII::one_file_per_group     = "yes"
IOASCII::output_symmetry_points = "no"
IOASCII::out3D_ghosts           = "no"
IOASCII::out3D_outer_ghosts     = "no"
IOASCII::out1D_vars             = "
        ADMBase::lapse
        ADMBase::shift
        ADMBase::metric
        ADMBase::curv
        HydroBase::rho
        HydroBase::vel
        HydroBase::eps
        HydroBase::press
        GRHydro::dens
        GRHydro::scon
        GRHydro::tau
        ML_ADMConstraints::ML_Ham
        ML_ADMConstraints::ML_Mom
        Refluxing::flux_weight_fine
        Refluxing::flux_weight_coarse
        Refluxing::densflux_register_fine
        Refluxing::densflux_register_coarse
        Refluxing::densflux_stored
        Refluxing::densflux_correction
        Refluxing::dens_correction_total
        Refluxing::sconflux_register_fine
        Refluxing::sconflux_register_coarse
        Refluxing::sconflux_stored
        Refluxing::sconflux_correction
        Refluxing::scon_correction_total
        Refluxing::tauflux_register_fine
        Refluxing::tauflux_register_coarse
        Refluxing::tauflux_stored
        Refluxing::tauflux_correction
        Refluxing::tau_correction_total
"
