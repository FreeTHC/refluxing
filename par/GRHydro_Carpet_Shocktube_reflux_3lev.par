ActiveThorns    =       "time
                         symbase
                         aeilocalinterp
                         carpetinterp
                         carpet
                         carpetlib
                         carpetregrid2
                         carpetreduce
                         carpetslab
                         cartgrid3d
                         coordbase
                         mol
                         boundary
                         admbase
                         staticconformal
                         spacemask
                         admcoupling
                         coordgauge
                         admmacros
                         constants
                         initbase
                         tmunubase
                         loopcontrol
                         hydrobase
                         ioutil
                         formaline
                         timerreport
                         nanchecker
                        "

# EOS
ActiveThorns     =      "EOS_Omni
                        "
# Hydro
ActiveThorns     =      "grhydro
                         grhydro_initdata
                         refluxing
                        "

# I/O
ActiveThorns     =      "carpetiobasic
                         carpetioascii
                         carpetioscalar
                         carpetiohdf5
                        "


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# I/O
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

carpetioscalar::outScalar_vars  =       "hydrobase::rho
                                         hydrobase::vel
                                         hydrobase::eps
                                         hydrobase::press
                                         grhydro::dens
                                         grhydro::scon
                                         grhydro::tau"

IOBasic::outInfo_vars           =       "hydrobase::rho
                                         hydrobase::vel[0]"

IOASCII::out1D_vars             =       "grid::coordinates
                                         carpetreduce::weight
                                         hydrobase::rho
                                         hydrobase::vel
                                         hydrobase::eps
                                         hydrobase::press
                                         grhydro::dens
                                         grhydro::scon
                                         grhydro::tau"

IO::out_dir                                  = $parfile
io::recover                                  = no
carpet::enable_all_storage                   = no

carpetioscalar::outScalar_every              =  1
IOASCII::out1D_every                         =  1
IOBasic::outInfo_every                       =  1

carpetioascii::out2D_every                   = 128

iohdf5::out_every                            =  -1
iohdf5::checkpoint                           = no
io::checkpoint_every                         = -1


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Timer
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TimerReport::output_schedule_timers          = no
TimerReport::n_top_timers                    = 20


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Initialization
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Carpet::init_fill_timelevels                 = yes

grhydro_initdata::shocktube_type        =       "xshock"
grhydro_initdata::shock_xpos            =       0.0
grhydro_initdata::shock_case            =       "sod"


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# AMR and Grid Setup
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Cactus::cctk_full_warnings                   = yes
carpet::veryverbose                          = no
carpet::verbose                              = no

carpet::max_refinement_levels                = 3
carpet::use_buffer_zones                     = yes
Carpet::refinement_centering                 = "cell"
Carpet::prolongation_order_space             =  1
Carpet::prolongation_order_time              =  1

Carpet::poison_new_timelevels = yes
CarpetLib::poison_new_memory  = yes


cartgrid3d::type                             = "coordbase"
cartgrid3d::domain                           = "full"
cartgrid3d::avoid_origin                     = no

coordbase::xmin                              = -1.0
coordbase::xmax                              = +1.0
coordbase::ymin                              = -0.0125
coordbase::ymax                              = +0.0125
coordbase::zmin                              = -0.0125
coordbase::zmax                              = +0.0125
coordbase::dx                                =  0.025
coordbase::dy                                =  0.025
coordbase::dz                                =  0.025

CoordBase::boundary_staggered_x_lower        = yes
CoordBase::boundary_staggered_y_lower        = yes
CoordBase::boundary_staggered_z_lower        = yes
CoordBase::boundary_staggered_x_upper        = yes
CoordBase::boundary_staggered_y_upper        = yes
CoordBase::boundary_staggered_z_upper        = yes

driver::ghost_size                           = 3

cactus::cctk_itlast                          = 200

Carpet::domain_from_coordbase                = yes

CoordBase::boundary_size_x_lower             = 3
CoordBase::boundary_size_y_lower             = 3
CoordBase::boundary_size_z_lower             = 3
CoordBase::boundary_size_x_upper             = 3
CoordBase::boundary_size_y_upper             = 3
CoordBase::boundary_size_z_upper             = 3

CarpetRegrid2::regrid_every                  = 0
CarpetRegrid2::verbose                       = yes
CarpetRegrid2::snap_to_coarse                = yes

CarpetRegrid2::num_centres                   = 1
CarpetRegrid2::num_levels_1                  = 3
CarpetRegrid2::position_x_1                  = 0.0
CarpetRegrid2::radius_1[1]                   = 0.2
CarpetRegrid2::radius_1[2]                   = 0.1

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Hydro
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

hydrobase::timelevels                        = 3
hydrobase::evolution_method                  = "grhydro"
hydrobase::prolongation_type                 = "Lagrange"
hydrobase::initial_hydro                     = "shocktube"

grhydro::grhydro_maxnumevolvedvars           = 5
grhydro::grhydro_maxnumsandrvars             = 16
grhydro::evolve_tracer                       = no
grhydro::number_of_tracers                   = 0

grhydro::grhydro_rho_central                 = 1.61930347e-08
grhydro::riemann_solver                      = "HLLE"
grhydro::grhydro_eos_type                    = "General"
grhydro::recon_method                        = "ppm"
grhydro::tvd_limiter                         = "vanleerMC2"

grhydro::ppm_detect                          = "yes"
grhydro::grhydro_stencil                     = 3
grhydro::bound                               = "flat"

EOS_Omni::poly_gamma                         = 1.4
EOS_Omni::poly_gamma_ini                     = 1.4
EOS_Omni::poly_k                             = 1
EOS_Omni::gl_gamma                           = 1.4

# Atmosphere
SpaceMask::use_mask                          =  yes

grhydro::rho_rel_min                         =  1.e-9
grhydro::initial_atmosphere_factor           =  1.e2
grhydro::initial_rho_abs_min                 =  5e-17

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Timestepping and MoL
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

time::dtfac                                  = 0.5
mol::ode_method                              ="icn"


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Curvature
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

admbase::evolution_method                    =  "none"
admbase::metric_type                         =  "physical"

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Check for NaNs
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

NaNChecker::check_every                      = 1
NaNChecker::check_vars                       = "grhydro::dens"
